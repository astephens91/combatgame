let gameActive = true;

function Characters(hp, hitPercentage, damage, defMod, armor){
    this.hp = hp;
    this.hitPercentage = hitPercentage;
    this.damage = damage;
    this.defMod = defMod;
    this.armor = armor;
}

Characters.prototype.showStats = function(){
    return ("HP: " + this.hp + "<br><br>" + "Damage: " + this.damage + "<br><br>" + "Armor: " + this.armor)
}

Characters.prototype.punch = function(enemy){
    hitPerc = (this.hitPercentage - enemy.defMod) / 100;
    damage = 0;
    if (Math.random() < hitPerc){
        for (let index = 0; index< this.damage; index++){
            damage += Math.floor(Math.random() * 11)
        }
    }
    
    enemy.hp -= (damage - enemy.armor);
    
    if (bulk.hp <= 0){
        document.getElementById("gameUpdates").innerHTML = "<h2 class='chrome'>BULK-TORY</h2>"
        let bulkImage = document.getElementById("Bulk")
        let pasta = document.createElement("img")
        pasta.setAttribute("src", "Images/pasta.gif")
        pasta.setAttribute("alt", "Pasta")
        pasta.setAttribute("height", "300")
        pasta.setAttribute("width", "350")
        gameActive = false;
        victorySound.play()
        bulkImage.parentNode.replaceChild(pasta, bulkImage)

    }
    else if (hero.hp <= 0){
        document.getElementById("gameUpdates").innerHTML = "<h2 class='chrome'>BULKED</h2>"
        let heroImage = document.getElementById("Hero")
        let tombstone = document.createElement("img")
        tombstone.setAttribute("src", "Images/TOMBSTONE.png")
        tombstone.setAttribute("alt", "Tombstone")
        tombstone.setAttribute("height", "300")
        tombstone.setAttribute("width", "350")
        gameActive = false;
        failhorn.play()
        heroImage.parentNode.replaceChild(tombstone, heroImage)
        
    }
}

function Hero (name){
    Characters.call(this, 100, 50, 7, 3, 3)
    this.name=name;
}
function Bulk (name){
    Characters.call(this, 120, 50, 10, 1, 1)
    this.name=name;
}

Hero.prototype = Object.create(Characters.prototype);
Hero.prototype.constructor = Hero;
Bulk.prototype = Object.create(Characters.prototype)
Bulk.prototype.constructor = Bulk;
let hero = new Hero("NinjaRobotPirate 'andy'")
let bulk = new Bulk("Bulk Brogan")
const punchSound = new Audio("Audio/punchSound.mp3")
const victorySound = new Audio("Audio/victory.mp3")
const failhorn = new Audio("Audio/failhorn.mp3")
const battlemusic = new Audio("Audio/battlemusic.mp3")


playerOneStatus = document.getElementById("playerOneStats");
playerOneStatus.innerHTML = hero.showStats();
playerTwoStatus = document.getElementById("playerTwoStats");
playerTwoStatus.innerHTML = bulk.showStats()



function fight(){
    if (gameActive == true){
    hero.punch(bulk);
    bulk.punch(hero);
    playerOneStats.innerHTML = hero.showStats();
    playerTwoStatus.innerHTML = bulk.showStats();
    punchSound.play()
    }
}